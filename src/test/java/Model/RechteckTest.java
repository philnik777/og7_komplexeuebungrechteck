package Model;

import Controller.BunteRechteckeController;
import org.junit.Test;

public class RechteckTest
{
    @Test
    public void Test()
    {
        BunteRechteckeController controller = new BunteRechteckeController();
        int[][] zahlen =
        {
                {10, 10, 30, 40},
                {25, 25, 100, 20},
                {260, 10, 200, 100},
                {5, 500, 300, 25},
                {100, 100, 100, 100},
                {-10, -10, -200, -100}
        };

        for (final int[] zahl : zahlen)
        {
            Rechteck rechteck = new Rechteck();
            rechteck.setX(zahl[0]);
            rechteck.setY(zahl[1]);
            rechteck.setBreite(zahl[2]);
            rechteck.setHoehe(zahl[3]);

            assert rechteck.toString().equals("Rechteck [x=" + zahl[0] +
                    ", y=" + zahl[1] +
                    ", breite=" + Math.abs(zahl[2]) +
                    ", hoehe=" + Math.abs(zahl[3]) + "]");
            controller.add(rechteck);

            Punkt a = new Punkt();
            Punkt b = new Punkt();
        }

        int[][] zahlen2 =
        {
                {200, 200, 200, 200},
                {800, 400, 20, 20},
                {800, 450, 20, 20},
                {850, 400, 20, 20},
                {855, 455, 25, 25},
                {-4, -5, -50, -200}
        };

        for (final int[] zahl : zahlen2)
        {
            Rechteck rechteck = new Rechteck(zahl[0], zahl[1], zahl[2], zahl[3]);

            assert rechteck.toString().equals("Rechteck [x=" + zahl[0] +
                    ", y=" + zahl[1] +
                    ", breite=" + Math.abs(zahl[2]) +
                    ", hoehe=" + Math.abs(zahl[3]) + "]");
            controller.add(rechteck);
        }

        assert controller.toString().equals("BunteRechteckeController [rechtecke=[" +
                "Rechteck [x=10, y=10, breite=30, hoehe=40], " +
                "Rechteck [x=25, y=25, breite=100, hoehe=20], " +
                "Rechteck [x=260, y=10, breite=200, hoehe=100], " +
                "Rechteck [x=5, y=500, breite=300, hoehe=25], " +
                "Rechteck [x=100, y=100, breite=100, hoehe=100], " +
                "Rechteck [x=-10, y=-10, breite=200, hoehe=100], " +
                "Rechteck [x=200, y=200, breite=200, hoehe=200], " +
                "Rechteck [x=800, y=400, breite=20, hoehe=20], " +
                "Rechteck [x=800, y=450, breite=20, hoehe=20], " +
                "Rechteck [x=850, y=400, breite=20, hoehe=20], " +
                "Rechteck [x=855, y=455, breite=25, hoehe=25], " +
                "Rechteck [x=-4, y=-5, breite=50, hoehe=200], " +
                "]]");

        Rechteck testEck = new Rechteck(0, 0, 1200, 1000);

        for (int i = 0; i < 500000; i++)
            assert testEck.enthaelt(Rechteck.generiereZufallsRechtck());

        for (int i = 0; i < 50000; i++)
            controller.generiereZufallsRechteck(25);
    }
}
