package Controller;

import Model.MySQLDatabase;
import Model.Rechteck;
import View.RechteckInputGUI;

import javax.swing.*;
import java.util.LinkedList;

public class BunteRechteckeController
{
    private LinkedList<Rechteck> rechtecke;
    private MySQLDatabase database;

    public BunteRechteckeController()
    {
        rechtecke = new LinkedList<>();
        database = new MySQLDatabase();
        rechtecke = database.getAlleRechtecke();
    }

    public void add(Rechteck rechteck)
    {
        rechtecke.add(rechteck);
        database.rechteckEintragen(rechteck);
    }

    public LinkedList<Rechteck> getRechtecke()
    {
        return rechtecke;
    }

    public void reset()
    {
        rechtecke = new LinkedList<>();
    }

    @Override
    public String toString()
    {
        String returnVar = "BunteRechteckeController [rechtecke=[";

        for (Rechteck rechteck : rechtecke)
            returnVar += rechteck.toString() + ", ";

        returnVar += "]]";
        return returnVar;
    }

    public void generiereZufallsRechteck(int anzahl)
    {
        reset();
        for (int i = 0; i < anzahl; i++)
            this.add(Rechteck.generiereZufallsRechtck());
    }

    public void rechteckHinzufuegen(JPanel pane)
    {
        RechteckInputGUI rechteckInputGUI = new RechteckInputGUI();
        rechteckInputGUI.addActionListener(e ->
        {
            add(rechteckInputGUI.getRechteck());
            pane.revalidate();
        });
    }
}
