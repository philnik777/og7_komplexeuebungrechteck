package View;

import Controller.BunteRechteckeController;
import Model.Rechteck;

import javax.swing.*;
import java.awt.*;

public class Zeichenflaeche extends JPanel
{
    private BunteRechteckeController controller;

    public Zeichenflaeche(BunteRechteckeController controller)
    {
        this.controller = controller;
    }

    @Override
    public void paintComponent(Graphics g)
    {
        g.setColor(Color.BLACK);
        for (Rechteck rechteck: controller.getRechtecke())
            g.drawRect(rechteck.getX(), rechteck.getY(), rechteck.getBreite(), rechteck.getHoehe());
    }
}
