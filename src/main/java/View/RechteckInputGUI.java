package View;

import Controller.BunteRechteckeController;
import Model.Rechteck;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class RechteckInputGUI
{
    private JTextArea textFields[] = new JTextArea[4];
    JButton button = new JButton();

    public RechteckInputGUI()
    {
        for (int i = 0; i < textFields.length; i++)
            textFields[i] = new JTextArea();

        {
            JFrame frame1 = new JFrame();
            frame1.setTitle("Rechtecke");
            frame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

            JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(9, 1));


            panel.add(new JLabel("X: "));
            panel.add(textFields[0]);

            panel.add(new JLabel("Y: "));
            panel.add(textFields[1]);

            panel.add(new JLabel("Height: "));
            panel.add(textFields[2]);

            panel.add(new JLabel("Width: "));
            panel.add(textFields[3]);

            panel.add(button);

            frame1.add(panel);

            frame1.pack();
            frame1.setVisible(true);
        }
    }

    public Rechteck getRechteck()
    {
        return new Rechteck(Integer.parseInt(textFields[0].getText()),
                Integer.parseInt(textFields[1].getText()),
                Integer.parseInt(textFields[2].getText()),
                Integer.parseInt(textFields[3].getText()));
    }

    public void addActionListener(ActionListener listener)
    {
        button.addActionListener(listener);
    }
}
