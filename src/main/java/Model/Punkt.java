package Model;

import java.util.HashMap;
import java.util.Objects;

public class Punkt
{
    private int x;
    private int y;

    public Punkt()
    {
        x = y = 0;
    }

    public Punkt(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        HashMap<String, String> map = new HashMap<>();

        //map["Banane"] = "Banane";

        return x;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public int getY()
    {
        return y;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Punkt punkt = (Punkt) o;
        return x == punkt.x &&
                y == punkt.y;
    }

    @Override
    public String toString()
    {
        return "Punkt{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
