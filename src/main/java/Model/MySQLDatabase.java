package Model;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class MySQLDatabase
{
    private final String driver = "com.mysql.cj.jdbc.Driver";
    private final String url = "jdbc:mysql://localhost/rechteck?useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String user = "root";
    private final String password = "";

    public void rechteckEintragen(Rechteck r)
    {
        try
        {
            Class.forName(driver);

            Connection connection = DriverManager.getConnection(url, user, password);

            String sql = "INSERT INTO T_Rechtecke (x, y, breite, hoehe) VALUES" +
                    " (?, ?, ?, ?);";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, r.getX());
            preparedStatement.setInt(2, r.getY());
            preparedStatement.setInt(3, r.getBreite());
            preparedStatement.setInt(4, r.getHoehe());
            preparedStatement.executeUpdate();

            connection.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public LinkedList<Rechteck> getAlleRechtecke()
    {
        LinkedList rechtecke = new LinkedList();

        try {
            Class.forName(driver);
            Connection connection = DriverManager.getConnection(url, user, password);
            String sql = "SELECT * FROM T_Rechtecke";

            ResultSet resultSet = connection.createStatement().executeQuery(sql);

            while (resultSet.next())
            {
                rechtecke.add(new Rechteck(resultSet.getInt(2),
                        resultSet.getInt(3),
                        resultSet.getInt(4),
                        resultSet.getInt(5)));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return rechtecke;
    }
}
