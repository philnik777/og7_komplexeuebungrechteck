package Model;

import Controller.BunteRechteckeController;
import View.Zeichenflaeche;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class BunteRechteckGUI_ extends JFrame
{
    private JPanel pane;
    private JMenuBar menuBar;
    private JMenu menu;
    private JMenuItem menuItemNeuesRechteck;
    private BunteRechteckeController brc;

    public static void main(String[] args)
    {
        BunteRechteckGUI_ frame = new BunteRechteckGUI_();
        try {
            frame.run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void run() throws InterruptedException
    {
        while (true)
        {
            Thread.sleep(100);
            this.revalidate();
            this.repaint();
        }
    }

    public BunteRechteckGUI_()
    {
        brc = new BunteRechteckeController();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, 960, 540);
        pane = new Zeichenflaeche(brc);
        pane.setBorder(new EmptyBorder(5, 5, 5, 5));
        pane.setLayout(new BorderLayout(0, 0));
        setContentPane(pane);

        menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        menu = new JMenu("Hinzufügen");
        menuBar.add(menu);

        menuItemNeuesRechteck = new JMenuItem("Rechtech hinzufügen");
        menuItemNeuesRechteck.addActionListener(e -> brc.rechteckHinzufuegen(pane));
        menu.add(menuItemNeuesRechteck);

        setVisible(true);
    }
}
