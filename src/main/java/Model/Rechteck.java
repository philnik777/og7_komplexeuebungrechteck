package Model;

import java.util.Random;

public class Rechteck {
    private int x;
    private int y;
    private int breite;
    private int hoehe;

    public Rechteck()
    {
        x = 0;
        y = x;
        breite = y;
        hoehe = breite;
    }

    public Rechteck(int x, int y, int breite, int hoehe)
    {
        this.x = x;
        this.y = y;
        setBreite(breite);
        setHoehe(hoehe);
    }

    public int getX()
    {
        return x;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public int getY()
    {
        return y;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    public int getBreite()
    {
        return breite;
    }

    public void setBreite(int breite)
    {
        this.breite = Math.abs(breite);
    }

    public int getHoehe()
    {
        return hoehe;
    }

    public void setHoehe(int hoehe)
    {
        this.hoehe = Math.abs(hoehe);
    }

    public boolean enthaelt(int x, int y)
    {
        return x >= this.x && x <= this.x + breite && y >= this.y && y <= this.y + hoehe;
    }

    public boolean enthaelt(Punkt p)
    {
        return enthaelt(p.getX(), p.getY());
    }

    public boolean enthaelt(Rechteck rechteck)
    {
        return enthaelt(rechteck.x, rechteck.y) && enthaelt(rechteck.x + rechteck.breite, rechteck.y + rechteck.hoehe);
    }

    public static Rechteck generiereZufallsRechtck()
    {
        Rechteck rechteck = new Rechteck();

        Random random = new Random();

        rechteck.x = random.nextInt(1000);
        rechteck.y = random.nextInt(800);

        rechteck.breite = random.nextInt(1000);
        rechteck.hoehe = random.nextInt(800);

        if (rechteck.breite == 0)
            rechteck.breite = 10;

        if (rechteck.hoehe == 0)
            rechteck.hoehe = 10;

        if (rechteck.x + rechteck.breite > 1200)
            rechteck.breite = 1200 - rechteck.x;


        if (rechteck.y + rechteck.hoehe > 1000)
            rechteck.hoehe = 1000 - rechteck.y;

        return rechteck;
    }

    @Override
    public String toString()
    {
        return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + "]";
    }
}
